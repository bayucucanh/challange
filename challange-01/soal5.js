function getSplitName(personName) {

  if (typeof personName === 'number') {
    return 'ERROR: Yang anda masukan adalah number';
  } else {
    // Membuat String menjadi Array lalu dipisahkan setiap mengandung spasi
    let splitFullName = personName.split(' ');
    if (splitFullName.length > 3) {
      return 'ERROR: Nama terlalu panjang';
    // Jika nama panjangnya 3
    } else if (splitFullName.length === 3) {
      hasilSplit = {
        firstName: splitFullName[0],
        middleName: splitFullName.slice(1, -1).join(''),
        lastName: splitFullName[splitFullName.length -1]
      }
    } else if (splitFullName.length < 2) {
      hasilSplit = {
        firstName: splitFullName[0],
        middleName: null,
        lastName: null
      }
    } else {
      hasilSplit = {
        firstName: splitFullName[0],
        middleName: null,
        lastName: splitFullName[splitFullName.length -1]
      }
    }
    return hasilSplit
  }
} 

console.log(getSplitName('Aldi Daniela Pranata'));
console.log(getSplitName('Dwi Kuncoro'));
console.log(getSplitName('Aurora'));
console.log(getSplitName('Aurora Aureliya Sukma Darma'));
console.log(getSplitName(0));