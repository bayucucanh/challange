const dataPenjualanPakAdi = [
  {
  namaProduct: 'Sepatu Futsal Nike Vapor Academy 9',
  hargaSatuan: 760000,
  kategori: 'Sepatu Sport',
  totalTerjual: 90
  },
  {
    namaProduct: 'Sepatu Warrior Tristan Black Brown High - Original',
    hargaSatuan: 960000,
    kategori: 'Sepatu Sneaker',
    totalTerjual: 37
  },
  {
    namaProduct: 'Sepatu Warrior Tristan Maroon High - Original',
    hargaSatuan: 360000,
    kategori: 'Sepatu Sneaker',
    totalTerjual: 90
  },
  {
    namaProduct: 'Sepatu Warrior Rainbow Tosca Cordury - [BNIB] Original',
    hargaSatuan: 120000,
    kategori: 'Sepatu Sport',
    totalTerjual: 90
  },
]

function hitungTotalPenjualan(dataPenjualan) {
  let totalPenjualan = 0;

  if(Array.isArray(dataPenjualan)) {
    for (i = 0; i < dataPenjualan.length; i++) {
      totalPenjualan += dataPenjualan[i].totalTerjual;
    }
  } else {
    return 'ERROR: Tipe data yang dimasukan bukan array';
  }

  
  return totalPenjualan;
}

console.log(hitungTotalPenjualan(dataPenjualanPakAdi));
console.log(hitungTotalPenjualan());
console.log(hitungTotalPenjualan(2));