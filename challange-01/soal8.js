const dataPenjualanNovel = [
  {
    idProduk: 'BOOK002421',
    namaProduk: 'Pulang - Pergi',
    penulis: 'Tere Liye',
    hargaBeli: 60000,
    hargaJual: 86000,
    totalTerjual: 150,
    sisaStock: 17,
  },
  {
    idProduk: 'BOOK002351',
    namaProduk: 'Selamat Tinggal',
    penulis: 'Tere Liye',
    hargaBeli: 75000,
    hargaJual: 103000,
    totalTerjual: 171,
    sisaStock: 20,
  },
  {
    idProduk: 'BOOK002941',
    namaProduk: 'Garis Waktu',
    penulis: 'Fiersa Besari',
    hargaBeli: 67000,
    hargaJual: 99000,
    totalTerjual: 213,
    sisaStock: 5,
  },
  {
    idProduk: 'BOOK002942',
    namaProduk: 'Laskar Pelangi',
    penulis: 'Andrea Hirata',
    hargaBeli: 55000,
    hargaJual: 68000,
    totalTerjual: 20,
    sisaStock: 56,
  },
]

function getInfoPenjualan(dataPenjualan) {
  let infoPenjualan = {}
  let modal = 0,
      keuntungan = 0,
      persentaseKeuntungan = 0,
      namaProdukTerlaris, 
      namaPenulisTerlaris;

  // Mencari data totalTerjual yang paling besar
  let terlaris = Math.max(...dataPenjualan.map((e) => e.totalTerjual));
  for (let i = 0; i < dataPenjualan.length; i++) {
    modal += dataPenjualan[i].hargaBeli * (dataPenjualan[i].totalTerjual + dataPenjualan[i].sisaStock);
    keuntungan += (dataPenjualan[i].hargaJual - dataPenjualan[i].hargaBeli ) * dataPenjualan[i].totalTerjual;
    // Jika arr dataPenjualan ke i yang memiliki properti totalTerjual sama dengan terlaris
    if (dataPenjualan[i].totalTerjual === terlaris) {
      // Maka isi variabel namaProdukTerlaris dengan arr dataPenjualan ke i yang memiliki properti namaProduk
      namaProdukTerlaris = dataPenjualan[i].namaProduk;
       // Maka isi variabel namaPenulisTerlaris dengan arr dataPenjualan ke i yang memiliki properti penulis     
      namaPenulisTerlaris = dataPenjualan[i].penulis;
    }
  }
  // Persentase Keuntungan
  persentaseKeuntungan = (keuntungan / modal) * 100;

  // Fungsi untuk merubah number menjadi String dengan Format Rupiah
  function convertNumber (number) {
    // format : number dijadikan string lalu string tersebut dijadikan substring
    const format = number.toString().split('').reverse().join('');
    const convert = format.match(/\d{1,3}/g);
    const rp = 'Rp. ' + convert.join('.').split('').reverse().join('');
    return rp ;
  }

  // Masukan semua data keuntungan, modal, persentaseKeuntungan, namaProdukTerlaris, namaPenulisTerlaris
  infoPenjualan['totalKeuntungan'] = convertNumber(keuntungan);
  infoPenjualan['totalModal'] = convertNumber(modal);
  infoPenjualan['persentaseKeuntungan'] = persentaseKeuntungan.toFixed(1) + '%';
  infoPenjualan['produkBukuTerlaris'] = namaProdukTerlaris;
  infoPenjualan['penulisTerlaris'] = namaPenulisTerlaris;

  return infoPenjualan;
}

console.log(getInfoPenjualan(dataPenjualanNovel));