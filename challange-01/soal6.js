function getAngkaTerbesarKedua(angka) {
      
  if (!angka && angka !== 0) {
    return 'ERROR : Parameter kosong'
  } else {
    if (angka === 0) {
      return 'ERROR : Yang anda masukan bukanlah array'
    } else {
      angka.sort((x, y) => {return x - y}).reverse()
      let angkaTerbesar = Math.max(...angka);
      let totalAngkaTerbesar = angka.filter((angka) => angka == angkaTerbesar).length
      return angka[totalAngkaTerbesar]
    }
  }
}

const dataAngka = [10, 10, 9, 4, 7, 7, 4, 3, 2, 2, 8, 9];
console.log(getAngkaTerbesarKedua(dataAngka));
console.log(getAngkaTerbesarKedua(0));
console.log(getAngkaTerbesarKedua());