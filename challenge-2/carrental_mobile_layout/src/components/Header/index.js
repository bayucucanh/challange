import {StyleSheet, Text, View, Dimensions, Image} from 'react-native';
import React from 'react';
import { WARNA_KEDUA, WARNA_HITAM } from '../../utils/constant';
import { PhotoProfile } from '../../assets';

const Header = () => {
  return (
    <View style={styles.header}>
      <Text style={styles.textName}>Hi, Bayu</Text>
      <View style={styles.profile}>
        <Text style={styles.textLocation}>Bandung, West Java</Text>
        <Image source={PhotoProfile} style={styles.photo} />
      </View>
    </View>
  );
};

export default Header;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;


const styles = StyleSheet.create({
  header: {
    width: windowWidth,
    height: windowHeight * 0.21,
    backgroundColor: WARNA_KEDUA,
    paddingHorizontal: 16,
  },
  textName: {
    fontSize: 14,
    fontWeight: '300',
    color: WARNA_HITAM,
  },
  profile: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  textLocation: {
    fontSize: 19,
    fontWeight: '700',
    color: WARNA_HITAM,
    paddingTop: 4,
  },
  photo: {
    width: 35,
    height: 35,
    alignItems: 'baseline',
    marginTop: -7,
  },
});
