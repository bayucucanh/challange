import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  Pressable,
  Image,
} from 'react-native';
import React from 'react';
import {WARNA_UTAMA} from '../../utils/constant';
import {Mercedes} from '../../assets';

const Banner = () => {
  return (
    <View style={styles.container}>
        <Text style={styles.teksBanner}>
          Sewa Mobil Berkualitas di kawasanmu
        </Text>
        {/* <Button title='Sewa Mobil' style={styles.btnSewa}/> */}
          <Pressable style={styles.btnSewa}>
            <Text style={styles.btnText}>Sewa Mobil</Text>
          </Pressable>
          <View style={styles.bgImage}></View>
          <Image source={Mercedes} style={styles.imageCar} />
    </View>
  );
};

export default Banner;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    backgroundColor: WARNA_UTAMA,
    padding: 24,
    marginHorizontal: 16,
    borderRadius: 10,
    height: windowHeight * 0.22,
    marginTop: -windowHeight * 0.12,
    position: 'relative',
    overflow: 'hidden'
  },
  teksBanner: {
    color: '#FFFFFF',
    fontSize: 17,
    lineHeight: 30,
    width: windowWidth * 0.48,
    // flex: 2,
    paddingBottom: 16,
  },
  btnSewa: {
    height: 28,
    width: 109,
    backgroundColor: '#5CB85F',
    paddingVertical: 4,
    paddingHorizontal: 16,
    borderRadius: 2,
  },
  btnText: {
    color: '#FFFFFF',
    fontWeight: '700'
  },
  imageCar: {
    width: 188,
    height: 115,
    position: 'absolute',
    right: 7,
    bottom: 0
  },
  bgImage: {
    width: 200,
    height: 64,
    backgroundColor: '#0D28A6',
    position: 'absolute',
    bottom: 0,
    right: 0,
    borderTopLeftRadius: 60
  }
});
