import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';
import React from 'react';
import {Xenia} from '../../assets';
import Feather from 'react-native-vector-icons/Feather';
import {WARNA_BACKGROUND, WARNA_HITAM} from '../../utils/constant';
import Gap from '../Gap';

const Mobil = ({merk, totalPenumpang, briefcase, price}) => {
  return (
    <TouchableOpacity style={styles.container}>
      <Image source={Xenia} style={styles.image} />
      <View>
        <Gap height={10}/>
        <Text style={styles.merk}>{merk}</Text>
        <View style={styles.tersewa}>
            <Feather name="users" />
            <Text style={styles.textJumlah}>{totalPenumpang}</Text>
            <Gap width={9}/>
            <Feather name="briefcase" />
            <Gap height={9}/>
            <Text style={styles.textJumlah}>{briefcase}</Text>
        </View>
        <Text style={styles.price}>{price}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default Mobil;

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 16,
    marginTop: 16,
    borderRadius: 2,
    height: 98,
    backgroundColor: WARNA_BACKGROUND,
    shadowColor: WARNA_HITAM,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
    flexDirection: 'row',
  },
  infoMobil: {
    paddingTop: 16,
    flexDirection: 'row'
  },
  image: {
    width: 50,
    height: 34,
    marginLeft: 16,
    marginRight: 22,
    marginTop: 24,
  },
  merk: {
    fontSize: 14,
    color: WARNA_HITAM,
    fontWeight: '500',
  },
  tersewa: {
    flexDirection: 'row',
    paddingTop: 6,
  },
  textJumlah: {
    fontSize: 10,
    paddingLeft: 5,
    paddingTop: 0,
  },
  price: {
    fontSize: 14,
    fontWeight: '500',
    color: '#5CB85F',
    paddingTop: 9,
  },
});
