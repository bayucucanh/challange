import React from 'react';
import { StyleSheet, Text, View } from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {Home, Splash, Pesanan, Akun} from '../pages';
import { BottomNavigator } from '../components';
import DaftarMobil from '../pages/DaftarMobil';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const MainApp = () => {
  return (
    <Tab.Navigator tabBar={props => <BottomNavigator{...props} />}>
        <Tab.Screen name="Home" component={Home} options={{headerShown: false}}/>
        <Tab.Screen name="Daftar Mobil" component={DaftarMobil}/>
        <Tab.Screen name="Akun" component={Akun}/>
      </Tab.Navigator>
  )
}
// Router digunakan untuk memasukan screen yang akan ditampilkan
const Router = () => {
  return (
    // initialRouteName='name' untuk menampilkan mana yang pertama di munculkan
    <Stack.Navigator initialRouteName='Splash'>
        <Stack.Screen name="MainApp" component={MainApp} options={{headerShown: false}}/>
        <Stack.Screen name="Splash" component={Splash} options={{headerShown: false}}/>
    </Stack.Navigator>
  )
}

export default Router

const styles = StyleSheet.create({})