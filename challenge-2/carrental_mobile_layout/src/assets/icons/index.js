import IconHome from './home1.svg';
import IconList from './list1.svg';
import IconUser from './user1.svg';
import IconHomeActive from './homeActive.svg';
import IconListActive from './listActive.svg';
import IconUserActive from './userActive.svg';
import IconTruck from './fi_truck.svg';
import IconBox from './fi_box.svg';
import IconKey from './fi_key.svg';
import IconCamera from './fi_camera.svg';
import IconUsers from './fi_users.svg'
import IconBriefCase from './fi_briefcase.svg'

export {
  IconHome, 
  IconList, 
  IconUser, 
  IconHomeActive, 
  IconListActive, 
  IconUserActive,
  IconTruck,
  IconBox,
  IconKey,
  IconCamera,
  IconUsers,
  IconBriefCase,
}