import { StyleSheet, Text, View, Image,  StatusBar } from 'react-native'
import React, {useEffect} from 'react'
// import Mercedes from '../../assets'
import {Mercedes} from '../../assets'
import { WARNA_UTAMA } from '../../utils/constant'

// props navigation didapat dari instalasi React Navigation
const Splash = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('MainApp')
    }, 3000)
  }, [navigation])

  return (
    <View style={styles.container}>
      <StatusBar 
      backgroundColor={WARNA_UTAMA} 
      hidden={true}/>
      
      <View style={styles.content}>
        <Text style={styles.title}>BCR</Text>
        <Text style={styles.title}>Binar Car Rental</Text>
      </View>
      <View style={styles.contentImage}>
        <View style={styles.backgroundCar}></View>
        <Image source={Mercedes} style={styles.image}/>
      </View>
    </View>
  )
}

export default Splash

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#091B6F', 
  },
  content: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 60
  },
  title: {
    fontSize: 29,
    fontWeight: '700',
    color: '#FFFFFF'
  },
  contentImage: {
    alignItems: 'center'
  },
  image: {
    width: 390,
    height: 208
  },
  backgroundCar: {
    position: 'absolute',
    right: 0,
    bottom: 0,
    width: '100%',
    height: 135,
    backgroundColor: '#D3D9FD',
    borderTopLeftRadius: 70
  }
})