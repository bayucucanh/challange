import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  Image,
  ScrollView,
  StatusBar
} from 'react-native';
import React from 'react';
import {PhotoProfile} from '../../assets';
import { WARNA_HITAM, WARNA_KEDUA} from '../../utils/constant';
import Banner from '../../components/Banner';
import Header from '../../components/Header';
import {ButtonIcon} from '../../components';
import Mobil from '../../components/Mobil';
import {useIsFocused} from '@react-navigation/native';
const Home = () => {
  return (
    <View style={styles.page}>
      <StatusBar
      backgroundColor={useIsFocused() ? WARNA_KEDUA : null}
      barStyle='dark-content'/>
      {/* ScrollView digunakan agar screen dapat di scroll
        showsHorizontalScrollIndicator={false} agar screen tidak dapat di scroll secara horizontal
      */}
      <ScrollView showsHorizontalScrollIndicator={false}>
        {/* Membuat Header pada halaman Home */}
        <Header />
        <Banner />
        <View style={styles.services}>
          <ButtonIcon title="Sewa Mobil" />
          <ButtonIcon title="Oleh-Oleh" />
          <ButtonIcon title="Penginapan" />
          <ButtonIcon title="Wisata" />
        </View>
        <View style={styles.mobilPilihan}>
          <Text style={styles.label}>Daftar Mobil Pilihan</Text>
        </View>
        <Mobil merk='Daihatsu Xenia' totalPenumpang='4' briefcase='2' price='Rp. 230.000'/>
        <Mobil merk='Daihatsu Xenia' totalPenumpang='4' briefcase='2' price='Rp. 230.000'/>
        <Mobil merk='Daihatsu Xenia' totalPenumpang='4' briefcase='2' price='Rp. 230.000'/>
        <Mobil merk='Daihatsu Xenia' totalPenumpang='4' briefcase='2' price='Rp. 230.000'/>
      </ScrollView>
    </View>
  );
};

export default Home;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  services: {
    // marginHorizontal: 4,
    flexDirection: 'row',
    marginTop: 28,
    justifyContent: 'space-around',
  },
  mobilPilihan: {
    paddingHorizontal: 16,
    flex: 1,
  },
  label: {
    paddingTop: 24,
    fontSize: 15,
    color: WARNA_HITAM,
    fontWeight: 'bold',
  },
});