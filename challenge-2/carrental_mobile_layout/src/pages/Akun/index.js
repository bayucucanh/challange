import { StyleSheet, Text, View, Image, TouchableOpacity, Dimensions, StatusBar } from 'react-native'
import React from 'react'
import { park1 } from '../../assets'
import { WARNA_BACKGROUND, WARNA_BUTTON, WARNA_HITAM, WARNA_UTAMA } from '../../utils/constant'
import Gap from '../../components/Gap'
import {useIsFocused} from '@react-navigation/native';

const Akun = () => {
  return (
    <View style={styles.container}>
      <StatusBar 
      backgroundColor={useIsFocused() ? WARNA_BACKGROUND : null} 
      barStyle='dark-content' />
      <Image source={park1} style={styles.imagePark}/>
      <Gap height={16}/>
      <Text style={styles.textInfo}>Upss kamu belum memiliki akun. Mulai buat akun agar transaksi di BCR lebih mudah</Text>
      <Gap height={16}/>
      <TouchableOpacity style={styles.btnRegister}>
        <Text style={styles.txtRegister}>Register</Text>
      </TouchableOpacity>
    </View>
  )
}

export default Akun
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: WARNA_BACKGROUND,
    justifyContent: 'center',
    alignItems: 'center'
  },
  imagePark: {
    width: windowWidth * 0.98,
    height: windowHeight * 0.3
  },
  textInfo: {
    fontSize: 14,
    color: WARNA_HITAM,
    lineHeight: 20,
    textAlign: 'center',
    width: windowWidth * 0.86
  },
  btnRegister: {
    backgroundColor: WARNA_BUTTON,
    height: 36,
    width: 81,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 2
  },
  txtRegister: {
    color: WARNA_BACKGROUND,
    fontWeight: '700'
  }
})