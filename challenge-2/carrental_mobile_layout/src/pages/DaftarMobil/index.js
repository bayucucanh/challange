import { StyleSheet, Text, View, ScrollView, StatusBar } from 'react-native'
import React from 'react'
import Mobil from '../../components/Mobil'
import { WARNA_BACKGROUND, WARNA_HITAM } from '../../utils/constant'
import {useIsFocused} from '@react-navigation/native';

const DaftarMobil = () => {
  return (
    <View style={styles.page}>
      <StatusBar 
      backgroundColor={useIsFocused() ? WARNA_BACKGROUND : null} 
      barStyle='dark-content' />
      <ScrollView showsHorizontalScrollIndicator={false}>
        <Mobil merk='Daihatsu Xenia' totalPenumpang='4' briefcase='2' price='Rp. 230.000'/>
        <Mobil merk='Daihatsu Xenia' totalPenumpang='4' briefcase='2' price='Rp. 230.000'/>
        <Mobil merk='Daihatsu Xenia' totalPenumpang='4' briefcase='2' price='Rp. 230.000'/>
        <Mobil merk='Daihatsu Xenia' totalPenumpang='4' briefcase='2' price='Rp. 230.000'/>
        <Mobil merk='Daihatsu Xenia' totalPenumpang='4' briefcase='2' price='Rp. 230.000'/>
        <Mobil merk='Daihatsu Xenia' totalPenumpang='4' briefcase='2' price='Rp. 230.000'/>
        <Mobil merk='Daihatsu Xenia' totalPenumpang='4' briefcase='2' price='Rp. 230.000'/>
      </ScrollView>
    </View>
  )
}

export default DaftarMobil

const styles = StyleSheet.create({
  page: {
    backgroundColor: '#FFFFFF'
  },  
  title: {
    marginHorizontal: 16,
    marginBottom: 15,
    fontWeight: '700',
    color: WARNA_HITAM,
    fontSize: 16
  }
})