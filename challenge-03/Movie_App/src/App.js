import { StyleSheet, Text, View } from 'react-native';
import React, { useState, useEffect } from 'react';
import NetInfo from '@react-native-community/netinfo';
import { NavigationContainer } from '@react-navigation/native';
import Router from './Router';
import ConnectionLost from './components/ConnectionLost';
// import InternetConnectionAlert from 'react-native-internet-connection-alert';

function App() {
  const [isOffline, setOfflineStatus] = useState(false);

  useEffect(() => {
    const removeNetInfoSubscription = NetInfo.addEventListener((state) => {
      const offline = !(state.isConnected && state.isInternetReachable);
      setOfflineStatus(offline);
    });
    return () => removeNetInfoSubscription();
  }, []);
  return (
    // <InternetConnectionAlert
    //   onChange={connectionState => {
    //     console.log('Connection State: ', connectionState);
    //   }}>
    <>
      {
      isOffline ? <ConnectionLost /> : <NavigationContainer><Router /></NavigationContainer>
    }
    </>

  // </InternetConnectionAlert>
  );
}

export default App;

const styles = StyleSheet.create({});
