import backButton from './backButton.svg';
import back from './back.png';
import shareIcon from './share-link.png';

export { backButton, back, shareIcon };
