import {
  StyleSheet, Text, View, Image, TouchableOpacity,
} from 'react-native';
import React, { useEffect, useState } from 'react';
import { moderateScale } from 'react-native-size-matters';
import { Rating, AirbnbRating } from 'react-native-ratings';
import axios from 'axios';
import Genre from '../Genre';

function CardList({
  title, release, rating, genreName, poster, btnDetail,
}) {
  const [dataGenre, setDataGenre] = useState([]);

  useEffect(() => {
    let isMounted = true;
    getGenre(genreName);
    return () => {
      isMounted = false;
    };

    async function getGenre(genreName) {
      try {
        const response = await axios.get(
          `http://code.aldipee.com/api/v1/movies/${genreName}`,
        );
        if (isMounted) setDataGenre(response.data);
      } catch (error) {
        console.error(error);
      }
    }
  }, []);

  return (
    <View style={styles.wrapper}>
      <Image source={poster} style={styles.cardImage} />
      <View style={styles.cardInfo}>
        {/* <Feather name='airplay' size={30}/> */}
        <View style={styles.rating}>
          <AirbnbRating
            count={5}
            showRating={false}
            isDisabled
            defaultRating={rating / 2}
            size={15}
            ratingContainerStyle={{
              alignSelf: 'flex-start',
              // backgroundColor: "#ede7f6"
            }}
          />
          {/* <Text style={styles.textInfo}>
            {rating}
            /10
          </Text> */}
        </View>

        <Text style={styles.title}>{title}</Text>
        <Text style={styles.textInfo}>
          Release on
          {release}
        </Text>
        <View style={styles.listGenre}>
          {dataGenre.genres?.map((item) => (
            <Genre key={item.id} genreName={item.name} />
          ))}
        </View>
        <TouchableOpacity style={styles.cardButton} onPress={btnDetail}>
          <Text style={{ color: 'white' }}>Show More</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

export default CardList;

const styles = StyleSheet.create({
  wrapper: {
    // borderWidth: 1,
    marginTop: moderateScale(12),
    flexDirection: 'row',
    backgroundColor: '#1e2746',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
    borderRadius: 5,
    position: 'relative',
  },
  cardImage: {
    width: moderateScale(110),
    height: moderateScale(180),
    borderRadius: moderateScale(5),
    // borderBottomEndRadius: 5
  },
  cardInfo: {
    paddingLeft: moderateScale(15),
    paddingTop: moderateScale(4),
  },
  title: {
    fontWeight: 'bold',
    color: '#fff',
    fontSize: moderateScale(15),
    width: moderateScale(200),
    flexWrap: 'wrap',
  },
  cardButton: {
    width: moderateScale(100),
    height: moderateScale(30),
    backgroundColor: '#007afe',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: moderateScale(5),
    marginTop: moderateScale(5),
    position: 'absolute',
    bottom: 5,
    left: 15,
  },
  textInfo: {
    color: '#5f6786',
    fontWeight: '500',
  },
  listGenre: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    width: moderateScale(220),
  },
  itemGenre: {
    // width: 60,
    height: moderateScale(20),
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: '#017bfe',
    borderRadius: moderateScale(5),
    marginRight: moderateScale(10),
  },
  textGenre: {
    color: '#fff',
    fontSize: moderateScale(12),
  },
  rating: {
    flexDirection: 'row',
    marginBottom: moderateScale(5),
  },
});
