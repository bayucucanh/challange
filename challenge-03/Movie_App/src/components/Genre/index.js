import {
  StyleSheet, Text, View, TouchableOpacity,
} from 'react-native';
import React from 'react';
import { moderateScale } from 'react-native-size-matters';

function Genre({ genreName }) {
  return (
    <View style={styles.wrapper}>
      <View style={styles.listGente}>
        <TouchableOpacity style={styles.itemGenre}>
          <Text style={{ color: '#fff' }}>{genreName}</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

export default Genre;

const styles = StyleSheet.create({
  itemGenre: {
    marginVertical: moderateScale(5),
    // width: 110,
    height: moderateScale(25),
    paddingHorizontal: moderateScale(7),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#5f6786',
    borderRadius: moderateScale(5),
    marginRight: moderateScale(10),
  },
});
