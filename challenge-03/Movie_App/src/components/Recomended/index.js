import {
  StyleSheet, Text, View, Image, ScrollView, TouchableOpacity,
} from 'react-native';
import React from 'react';
import { moderateScale, s } from 'react-native-size-matters';

function Recomended({ sourceImage, title, btnDetail }) {
  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.recomendedItem} onPress={btnDetail}>
        <Image source={sourceImage} style={styles.img} />
        <Text style={styles.title}>{title}</Text>
      </TouchableOpacity>
    </View>
  );
}

export default Recomended;

const styles = StyleSheet.create({
  container: {
    marginRight: moderateScale(8),
    marginVertical: moderateScale(16),
  },
  // recomendedItem: {

  //   marginTop: 10
  // },
  img: {
    width: moderateScale(120),
    height: moderateScale(140),
    borderRadius: moderateScale(8),
  },
  title: {
    flexWrap: 'wrap',
    width: moderateScale(120),
    marginTop: moderateScale(7),
    color: '#fff',
    textAlign: 'center',
  },
});
