import {
  StyleSheet, Text, View, Image,
} from 'react-native';
import React from 'react';
import { moderateScale } from 'react-native-size-matters';
import { NoInternet } from '../../assets';

function ConnectionLost() {
  return (
    <View style={styles.container}>
      <Image source={NoInternet} style={styles.imageConnection} />
      <Text style={styles.titleConnection}>No Internet Connection</Text>
      <Text style={styles.txtConnection}>Please check your internet connection it looks like you're not connected to the internet</Text>
    </View>
  );
}

export default ConnectionLost;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#141a31',
  },
  imageConnection: {
    width: moderateScale(300),
    height: moderateScale(300),
  },
  titleConnection: {
    color: '#fd9210',
    fontSize: moderateScale(18),
    fontWeight: 'bold',
    marginVertical: moderateScale(5),
  },
  txtConnection: {
    color: '#5f6786',
    textAlign: 'center',
    fontSize: 15,
    width: moderateScale(300),
  },
});
