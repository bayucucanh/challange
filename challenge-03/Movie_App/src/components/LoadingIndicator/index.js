import { StyleSheet, Text, View, ActivityIndicator } from 'react-native'
import React from 'react'

const Loading = () => {

  
  return (
    <View style={styles.container}>
      <ActivityIndicator size='large' color='white'/>
      <Text style={styles.text}>Loading</Text>
    </View>
  )
}

export default Loading

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#141a31',
    justifyContent: 'center',
    alignItems: 'center'
  },
  text: {
    color: '#fd9210',
    marginTop: 10,
    fontSize: 20,
    fontWeight: 'bold'
  }
})