import { StyleSheet, Text, View } from 'react-native';
import React from 'react';
import { moderateScale } from 'react-native-size-matters';

function Synopshis({ Synopshis }) {
  return (
    <View style={styles.wrapper}>
      <Text style={styles.title}>Synopshis</Text>
      <Text style={styles.synopshis}>{Synopshis}</Text>
    </View>
  );
}

export default Synopshis;

const styles = StyleSheet.create({
  wrapper: {
    margin: moderateScale(12),
  },
  title: {
    fontSize: moderateScale(17),
    fontWeight: 'bold',
    marginVertical: moderateScale(12),
    color: '#fd9210',
  },
  synopshis: {
    color: '#5f6786',
    fontWeight: '500',
  },
});
