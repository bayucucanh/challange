import {
  StyleSheet, Text, View, Image,
} from 'react-native';
import React from 'react';
import { moderateScale } from 'react-native-size-matters';

function ListArtist({ profileImage, profileName }) {
  return (
    <View style={styles.wrapper}>
      <View style={styles.listPhoto}>
        <Image
          source={profileImage}
          style={styles.photoArtist}
        />
        <Text style={styles.name}>{profileName}</Text>
      </View>
    </View>
  );
}

export default ListArtist;

const styles = StyleSheet.create({
  wrapper: {
    marginHorizontal: moderateScale(12),
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginVertical: moderateScale(12),
    color: '#000',
  },
  photoArtist: {
    width: moderateScale(94),
    height: moderateScale(150),
    borderRadius: moderateScale(5),
  },
  name: {
    color: '#fff',
    textAlign: 'center',
    flexWrap: 'wrap',
    width: moderateScale(90),
    marginVertical: moderateScale(5),
  },
});
