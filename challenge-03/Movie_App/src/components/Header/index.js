import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  TouchableOpacity,
  Image,
  Button,
} from 'react-native';
import React, { useNavigation } from 'react';
import { Rating, AirbnbRating } from 'react-native-ratings';
import { moderateScale } from 'react-native-size-matters';
import ShareSocial from 'react-native-share';
import ImgToBase64 from 'react-native-image-base64';
import { back, shareIcon } from '../../assets';

function Header({
  sourceImage, goBack, title, homepage,
}) {
  const onShare = async () => {
    // ImgToBase64.getBase64String(data.poster_path)
    //   .then(base64String => setPoster(base64String))
    //   .catch(err => console.log(err));

    // let poster = `data:image/jpeg:base64,${poster}`

    const shareOptions = {
      message: `Watch ${title} at the following link ${homepage}`,
      // url: poster
    };

    try {
      await ShareSocial.open(shareOptions);
    } catch (err) {
      console.log('Error => ', err);
    }
  };

  return (
    <View>
      <ImageBackground source={sourceImage} style={styles.bgImage}>
        <TouchableOpacity onPress={goBack} style={styles.btnGoBack}>
          <Image source={back} style={styles.btnImage} />
        </TouchableOpacity>
        <View style={{ flexDirection: 'row', marginRight: 5 }}>
          <Rating
            type="heart"
            tintColor="rgba(52, 52, 52, 5)"
            defaultRating={0}
            ratingCount={1}
            imageSize={35}
            style={{ margin: 12 }}
          />
          <TouchableOpacity onPress={onShare}>
            <Image source={shareIcon} style={[styles.btnImage, styles.shareIcon]} />
          </TouchableOpacity>
        </View>
      </ImageBackground>
    </View>
  );
}

export default Header;

const styles = StyleSheet.create({
  bgImage: {
    width: '100%',
    height: moderateScale(190),
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  btnGoBack: {
    marginHorizontal: 12,
    marginTop: 10,
  },
  btnImage: {
    width: moderateScale(35),
    height: moderateScale(35),
  },
  shareIcon: {
    marginVertical: 10,
    marginHorizontal: 12,
  },
});
