import {
  StyleSheet, Text, View, Image,
} from 'react-native';
import React from 'react';
import { moderateScale } from 'react-native-size-matters';
import { Rating, AirbnbRating } from 'react-native-ratings';
import CardList from '../CardList';

function Banner({
  poster, title, release, rating, tagline, status, runtime,
}) {
  return (
    <View style={styles.wrapper}>
      <Image
        // source={poster}
        source={poster}
        style={styles.cardImage}
      />
      <View style={styles.cardInfo}>
        <Text style={styles.title}>{title}</Text>
        <View style={styles.rating}>
          <AirbnbRating
            count={5}
            showRating={false}
            isDisabled
            defaultRating={rating / 2}
            size={15}
            ratingContainerStyle={{
              alignSelf: 'flex-start',
              // backgroundColor: "#ede7f6"
            }}
          />
          <Text style={{ color: '#fff', paddingLeft: 10, fontSize: 15 }}>
            {rating}
            /10
          </Text>
        </View>
        <Text style={styles.textInfo}>{tagline}</Text>
        <Text style={styles.textInfo}>
          {status}
          {' '}
          on
          {' '}
          {release}
        </Text>
        <Text style={styles.textInfo}>
          {runtime}
          {' '}
          Minutes
        </Text>
      </View>
    </View>
  );
}

export default Banner;

const styles = StyleSheet.create({
  wrapper: {
    // borderWidth: 1,
    marginTop: moderateScale(-75),
    marginHorizontal: moderateScale(12),
    flexDirection: 'row',
    backgroundColor: '#1e2746',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
    borderRadius: moderateScale(5),
    position: 'relative',
  },
  cardImage: {
    width: moderateScale(100),
    height: moderateScale(130),
    borderBottomLeftRadius: moderateScale(5),
    borderTopLeftRadius: moderateScale(5),
    // borderBottomEndRadius: 5
  },
  cardInfo: {
    paddingLeft: moderateScale(25),
    paddingTop: moderateScale(4),
  },
  title: {
    fontWeight: 'bold',
    color: '#fff',
    fontSize: 15,
    width: moderateScale(220),
    flexWrap: 'wrap',
    marginBottom: moderateScale(5),
    marginTop: moderateScale(5),
  },
  textInfo: {
    color: '#5f6786',
    fontWeight: '500',
    flexWrap: 'wrap',
    width: moderateScale(240),
  },
  rating: {
    flexDirection: 'row',
    marginTop: moderateScale(3),
    marginBottom: moderateScale(3),
  },
});
