import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  ImageBackground,
  Button,
  TouchableOpacity,
  RefreshControl,
  Share,
} from 'react-native';
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { moderateScale } from 'react-native-size-matters';
import Banner from '../../components/Banner';
import Genre from '../../components/Genre';
import Synopshis from '../../components/Synopshis';
import ListArtist from '../../components/ListArtist';
import Header from '../../components/Header';
import Loading from '../../components/LoadingIndicator';

function DetailFilm({ route, navigation }) {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const [refresh, setRefresh] = useState(false);
  // const [poster, setPoster] = useState('');
  const { id } = route.params;

  async function getData(id) {
    try {
      const response = await axios.get(
        `http://code.aldipee.com/api/v1/movies/${id}`,
      );
      setData(response.data);
    } catch (err) {
      console.error(err);
      setLoading(false)
      setRefresh(false)
    } finally {
      setLoading(false)
      setRefresh(false)
    }
  }

  useEffect(() => {
    getData(id);
    return () => {
      isMounted = false;
    };
  }, []);

  const onRefresh = () => {
    setRefresh(true);
    getData(id);
    setRefresh(false);
  };

  return (
    <>
      {isLoading? (
        <Loading />
      ) : ( 
    <ScrollView
      refreshControl={
        <RefreshControl refreshing={refresh} onRefresh={onRefresh} />
      }
    >
      <View style={styles.container}>
        <Header
          sourceImage={{ uri: data.backdrop_path }}
          goBack={() => navigation.goBack()}
          title={data.title}
          homepage={data.homepage}
        />
        <Banner
          poster={{ uri: data.poster_path }}
          title={data.title}
          tagline={data.tagline}
          status={data.status}
          release={data.release_date}
          rating={data.vote_average}
          runtime={data.runtime}
        />

        <Text style={styles.title}>Genres</Text>
        <View style={styles.listGenre}>
          {data.genres?.map((item) => (
            <Genre key={item.id} genreName={item.name} />
          ))}
        </View>

        <Synopshis Synopshis={data.overview} />
        <View style={styles.artist}>
          <Text style={styles.title}>Actors/Artist</Text>
          <View style={styles.listArtist}>
            {data.credits?.cast.map((item) => (
              <ListArtist
                key={item.id}
                profileImage={{ uri: item.profile_path }}
                profileName={item.name}
              />
            ))}
          </View>
        </View>
      </View>
    </ScrollView>
      )}
      </>
  );
}

export default DetailFilm;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#141a31',
  },
  title: {
    fontSize: moderateScale(17),
    fontWeight: 'bold',
    marginVertical: 12,
    marginHorizontal: 12,
    color: '#fd9210',
  },
  listGenre: {
    marginHorizontal: moderateScale(12),
    flexDirection: 'row',
    flexWrap: 'wrap',
    width: moderateScale(350),
  },
  image: {
    width: moderateScale(110),
    height: moderateScale(150),
    borderRadius: moderateScale(5),
    marginBottom: moderateScale(15),
  },
  listArtist: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
  },
});
