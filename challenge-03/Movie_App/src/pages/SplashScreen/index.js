import {
  StyleSheet, Text, View, Image, StatusBar,
} from 'react-native';
import React, { useEffect } from 'react';
import { Logo } from '../../assets';

function SplashScreen({ navigation }) {
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('HomeScreen');
    }, 3000);
  }, [navigation]);

  return (
    <View style={styles.container}>
      <StatusBar
        backgroundColor="#141a31"
      />
      {/* <Image source={Logo}/> */}
      <Image source={Logo} style={styles.image} />
      <Text style={styles.titleName}>Bayu Cucan Herdian</Text>
    </View>
  );
}

export default SplashScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#141a31',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
  },
  image: {
    width: 400,
    height: 400,
  },
  titleName: {
    color: '#fd9210',
    fontWeight: '500',
    position: 'absolute',
    fontSize: 15,
    bottom: 20,
    textTransform: 'capitalize',
  },
});
