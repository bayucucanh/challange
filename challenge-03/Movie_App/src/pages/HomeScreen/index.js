import {
  StyleSheet, Text, View, Button, ScrollView, StatusBar, RefreshControl,
} from 'react-native';
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { moderateScale } from 'react-native-size-matters';
import Recomended from '../../components/Recomended';
import CardList from '../../components/CardList';
import Loading from '../../components/LoadingIndicator';

function HomeScreen({ navigation }) {
  const [isLoading, setLoading] = useState(true);
  const [refresh, setRefresh] = useState(false);
  const [data, setData] = useState([]);

  function sorting_latest(x, y) {
    return (
      new Date(y.release_date).getTime() - new Date(x.release_date).getTime()
    );
  }

  
  const onRefresh = () => {
    setRefresh(true);
    getData();
  };

  async function getData() {
    try {
      await axios.get('http://code.aldipee.com/api/v1/movies/')
      .then((response) => {
        setData(response.data.results);
      })
    } catch (err) {
      console.error(err);
      setLoading(false)
      setRefresh(false)
    } finally {
      setLoading(false)
      setRefresh(false)
    }
  }

  useEffect(() => {
    let isMounted = true
    getData();
    return () => {
      isMounted = false;
    };
  });


  return (
    <>
      {isLoading? (
        <Loading />
      ) : ( 
    <View style={styles.container}>
      <StatusBar barStyle="light-content" backgroundColor="#141a31" />
      <ScrollView refreshControl={<RefreshControl refreshing={refresh} onRefresh={onRefresh} />}>
        <View style={styles.wrapper}>
          <Text style={styles.appName}>Movie Labs</Text>
          <View style={{ marginTop: 20 }}>
            <Text style={styles.title}>Recomended</Text>
            <ScrollView horizontal>
              <View style={styles.recommended}>
                {data.map((item) => (
                  <Recomended
                    key={item.id}
                    sourceImage={{ uri: item.poster_path }}
                    title={item.title}
                    btnDetail={() => navigation.navigate('DetailFilm', { id: item.id })}
                  />
                ))}
              </View>
            </ScrollView>
          </View>
          <View style={styles.latestUpload}>
            <Text style={styles.title}>Latest Upload</Text>
            <ScrollView>
              {data.sort(sorting_latest).map((item) => (
                <CardList
                  key={item.id}
                  title={item.title}
                  release={item.release_date}
                  rating={item.vote_average}
                  genreName={item.id}
                  poster={{ uri: item.poster_path }}
                  btnDetail={() => navigation.navigate('DetailFilm', { id: item.id })}
                />
              ))}
            </ScrollView>
          </View>
        </View>
      </ScrollView>
    </View>
    )}
    </>
  );
}

export default HomeScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#141a31',
  },
  appName: {
    fontSize: moderateScale(20),
    fontWeight: '700',
    color: '#fff',
    textAlign: 'center',
    marginTop: moderateScale(12),
  },
  recommended: {
    flexDirection: 'row',
  },
  wrapper: {
    marginHorizontal: moderateScale(12),
  },
  title: {
    // marginTop: 20,
    color: '#fd9210',
    fontWeight: 'bold',
    fontSize: moderateScale(17),
  },
  latestUpload: {
    marginBottom: moderateScale(10),
  },
});
